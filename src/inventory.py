from flask import Flask
from src.controllers.inventory_controller import InventoryController

app = Flask(__name__)
app.register_blueprint(InventoryController)
