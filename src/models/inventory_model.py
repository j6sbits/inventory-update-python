from pymongo import MongoClient

class InventoryModel():
  def __init__(self):
    self.client = MongoClient('mongodb://jarvis-mongo:27017')
    self.db = self.client.jarvis
    self.collection = self.db.inventories

  def update(self, inventory):
    self.collection.update_one({ 'product': inventory['product'] }, { '$set': { 'lote': inventory['lote'] }})
  